package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/evaluation"
	"github.com/sjwhitworth/golearn/knn"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("Usage: %s filename k\n", os.Args[0])
		return
	}

	datasetFile := os.Args[1]
	rawData, err := base.ParseCSVToInstances(datasetFile, false)
	if err != nil {
		panic(err)
	}

	k, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic(err)
	}

	cls := knn.NewKnnClassifier("euclidean", "linear", k)
	train, test := base.InstancesTrainTestSplit(rawData, 0.5)
	cls.Fit(train)

	p, err := cls.Predict(test)
	if err != nil {
		panic(err)
	}

	confusionMatrix, err := evaluation.GetConfusionMatrix(test, p)
	if err != nil {
		panic(err)
	}

	fmt.Println(evaluation.GetSummary(confusionMatrix))
}
